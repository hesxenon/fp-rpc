import { option, reader, task } from "fp-ts";
import { Handler } from "./Handler";
import { Message } from "./Message";
import { flow, identity, pipe } from "fp-ts/lib/function";

export type Layer<
  H1 extends Handler = Handler,
  H2 extends Handler = Handler
> = (handler: H1) => H2;

export namespace Layer {
  export const map = (
    mapRequest: (message: Message) => Message,
    mapResponse: (message: Message) => Message
  ): Layer =>
    reader.map((handler) =>
      flow(task.map(mapRequest), handler, task.map(mapResponse))
    );

  export const mapRequest = (fn: Parameters<typeof map>[0]) =>
    map(fn, identity);

  export const mapResponse = (fn: Parameters<typeof map>[1]) =>
    map(identity, fn);

  export const timebasedCache: Layer = reader.map((handler) => {
    const cache = new Map<string, { validThru: Date; response: Message }>();

    return task.flatMap((request) => {
      const key = JSON.stringify(request);

      return pipe(
        cache.get(key),
        option.fromPredicate(
          (cached): cached is NonNullable<typeof cached> =>
            cached != null && cached.validThru.valueOf() > Date.now()
        ),
        option.map(({ response }) => task.of(response)),
        option.getOrElse(() =>
          pipe(
            handler(task.of(request)),
            task.map((response) => {
              if (
                response.meta != null &&
                typeof response.meta === "object" &&
                "validThru" in response.meta &&
                typeof response.meta.validThru === "string"
              ) {
                const validThru = new Date(response.meta.validThru);
                cache.set(key, { validThru, response });
              }
              return response;
            })
          )
        )
      );
    });
  });
}
