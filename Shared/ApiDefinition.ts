import { type task } from "fp-ts";

export type ApiFunction = (arg: any) => task.Task<any>;
export type ApiDefinition = {
  [prop: string]: ApiFunction | ApiDefinition;
};


