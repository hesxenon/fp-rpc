import { reader, task } from "fp-ts";
import { Message } from "./Message";

export type Handler<
  M1 extends Message = Message,
  M2 extends Message = Message
> = reader.Reader<string, (getReq: task.Task<M1>) => task.Task<M2>>;
