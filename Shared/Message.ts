import { either } from "fp-ts";
import { ZodError, z } from "zod";

export interface Message<Data = unknown, Meta = unknown> {
  meta: Meta;
  data: Data;
}

export namespace Message {
  export type Raw = [id: string, path: string, message: Message];

  export const create = <Req extends Message>(req: Req) => req;
  export const fromData = <Data>(data: Data): Message<Data> =>
    create({ data, meta: undefined });

  export const setMeta =
    <A, B>(fn: (a: A) => B) =>
    <Data>(req: Message<Data, A>): Message<Data, B> => ({
      ...req,
      meta: fn(req.meta),
    });

  export const setData =
    <A, B>(fn: (a: A) => B) =>
    <Meta>(req: Message<A, Meta>): Message<B, Meta> => ({
      ...req,
      data: fn(req.data),
    });

  export const validate = <Schema extends z.Schema>(schema: Schema) =>
    setData((data) =>
      either.tryCatch(
        () => schema.parse(data) as z.infer<Schema>,
        (e) => e as ZodError
      )
    );
}
