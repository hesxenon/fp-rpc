export const ignore = () => {};

export const failWith = (message: string) => () => {
  throw new Error(message);
};

export const fail = (data: any) => {
  throw data;
};
