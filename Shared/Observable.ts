import { apply } from "fp-ts/lib/function";
import { ignore } from "./Global";

export interface Observer<A> {
  next: (value: A) => void;
}

export interface Observable<A> {
  subscribe: (subscriber: Observable.Subscriber<A>) => {
    unsubscribe: () => void;
  };
}

export namespace Observable {
  export type Subscriber<A> = (value: A) => void;
  export const create = <A>(
    setup: (observer: Observer<A>) => () => void
  ): Observable<A> => {
    const subscribers = [] as Array<Subscriber<A>>;

    let teardown = ignore;

    return {
      subscribe: (subscriber) => {
        if (subscribers.length === 0) {
          teardown = setup({
            next: (value) => {
              subscribers.forEach(apply(value));
            },
          });
        }
        subscribers.push(subscriber);
        return {
          unsubscribe: () => {
            subscribers.splice(subscribers.indexOf(subscriber), 1);
            if (subscribers.length === 0) {
              teardown();
            }
          },
        };
      },
    };
  };
}

export interface Subject<A> extends Observable<A>, Observer<A> {}

export namespace Subject {
  export const create = <A>(): Subject<A> => {
    const subscribers = [] as Array<Observable.Subscriber<A>>;

    return {
      next: (value) => subscribers.forEach(apply(value)),
      subscribe: (subscriber) => {
        subscribers.push(subscriber);
        return {
          unsubscribe: () => {
            subscribers.splice(subscribers.indexOf(subscriber), 1);
          },
        };
      },
    };
  };
}
