import { ApiDefinition, ApiFunction, Message } from "../Shared";
import { WebSocketHandler } from "bun";
import { task } from "fp-ts";
import { pipe, apply, flow } from "fp-ts/lib/function";

type ServerFunction<Fn extends ApiFunction = ApiFunction> =
  ReturnType<Fn> extends task.Task<infer Res>
    ? (req: Message<Parameters<Fn>[0]>) => task.Task<Message<Res>>
    : never;

export type Server<Api extends ApiDefinition> = {
  [k in keyof Api]: Api[k] extends ApiFunction
    ? ServerFunction<Api[k]>
    : Api[k] extends ApiDefinition
    ? Server<Api[k]>
    : never;
};

export namespace Server {
  export function create<Api extends ApiDefinition>(
    impl: Server<Api>
  ): WebSocketHandler {
    const serialize = JSON.stringify;
    const parse = JSON.parse;

    const handlers = {} as Record<string, ServerFunction>;

    return {
      message: (ws, stringified) => {
        const [id, path, message] = pipe(
          parse(stringified as string)
        ) as Message.Raw;

        const handler = (handlers[path] ??= (() => {
          // find handler at path
          const handler = path
            .split(".")
            .reduce(
              (cursor, part) =>
                cursor == null ? undefined : (cursor as ApiDefinition)[part],
              impl as undefined | ApiDefinition | ServerFunction
            );

          if (handler == null || !(typeof handler === "function")) {
            throw new Error(`could not find handler at path ${path}`);
          }
          return handler;
        })());

        pipe(
          handler(message),
          task.map(
            flow(
              (response) => [id, path, response],
              serialize,
              (response) => ws.send(response)
            )
          ),
          apply(undefined)
        );
      },
    };
  }
}
