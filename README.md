# fp-rpc

## Typesafe APIs without gaps

The motivation behind this project was to have an "api first" approach that works

1. without neglecting error handling
1. over websockets instead of classic HTTP.

## How to use

1. define a shared API, like this:

```typescript
import { type taskEither } from "fp-ts";

export type API = {
  ping: (
    message: string
  ) => taskEither.TaskEither<"empty message", `pong: ${string}`>; // either an error "empty message" or success "pong: ${string}"
};
```

2. implement the API on the server

```typescript
import { taskEither } from "fp-ts";
import { Server } from "fp-rpc/Server";

const api: API = {
  ping: (message) =>
    message === ""
      ? taskEither.left("empty message")
      : taskEither.right(`pong: ${message}`),
};

Bun.serve({
  /* see how to create a bun webserver here: https://bun.sh/docs/api/websockets#create-a-websocket-server */
  websocket: Server.create(api),
});
```

3. use the API on the client

```typescript
import { Client } from "fp-rpc/Client";
const api: API = Client.create<API>();

// solid-article.tsx
const [article] = createResource(api.articles.get(props.id));

return (
  <div>
    {pipe(
      article(),
      either.match(
        (error) =>
          patternmatch(error)
            .with("not found", () => <ArticleNotFound />)
            .with("payment required", () => <Paywall />)
            .exhaustive(),
        (article) => <Content article={article} />
      )
    )}
  </div>
);
```

## Reasoning

### Smaller Payloads

Since each HTTP request sends quite a lot of headers with it the size of a websocket message can be much smaller, focussing only on what's actually needed:

- an id to match a request to a response
- an identifier of the requested resource
- the data

### Transparent response handling

HTTP defines many different semantics by now - and all of them have their place, no doubt. But when it comes to "simulating" calls as if they were happening on the same system, in a typesafe system many of those semantics feel like a workaround, a mere "hack" to express a certain behaviour.

Take for example the good old 404 status code - the resource hasn't been found. If you're crossing language boundaries or can't really have typesafe APIs anyway it absolutely makes sense. But when was the last time you wrote a function like `getUser(id: string): "404" | User`?

Why would we limit the expressiveness of our type system to the HTTP status code _and_ pay the prize of a bigger payload size?

## fp-ts

Promises are great and all - but their errors are untyped. So the solution of this library is to rely on good old monads to express what you want to do.

What's that? You're scared of monads? Don't be, you've been using them all along. `somePromise.then((user) => name)` is very much similar to `pipe(someTask, Task.map((user) => user.name))`. Once you've got the hang of it you'll see that they're no different than promises or arrays.

## Contributing

To install dependencies:

```bash
bun install
```

To run:

```bash
bun --watch test
```

This project was created using `bun init` in bun v0.5.9. [Bun](https://bun.sh) is a fast all-in-one JavaScript runtime.
