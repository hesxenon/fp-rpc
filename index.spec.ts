import { Client } from "./Client";
import { Server } from "./Server";
import { ApiDefinition, Message, Layer, Subject, ignore } from "./Shared";
import { ServerWebSocket } from "bun";
import { expect, test } from "bun:test";
import { it } from "bun:test";
import { either, option, task, taskEither, taskOption } from "fp-ts";
import { flow, pipe } from "fp-ts/lib/function";
import { z } from "zod";
import { P, match } from "ts-pattern";

const create = <Api extends ApiDefinition>(
  impl: Server<Api>,
  layers: Layer[] = []
) => {
  const c2s = Subject.create<string>();
  const s2c = Subject.create<string>();

  const ws = {
    onmessage: ignore,
    send: (stringified) => c2s.next(stringified as string),
  } as Pick<WebSocket, "onmessage" | "send">;

  const server = Server.create(impl);

  c2s.subscribe((message) => {
    server.message(
      {
        send: (message) => {
          s2c.next(message as string);
          return 0;
        },
      } as ServerWebSocket,
      message
    );
  });

  s2c.subscribe((message) => {
    (ws as WebSocket).onmessage?.(
      new MessageEvent("", {
        data: message,
      })
    );
  });

  return Client.create<Api>({
    ws,
    layers,
  });
};

it("should resolve", async () => {
  type Api = {
    foo: () => task.Task<string>;
  };

  const client = create<Api>({
    foo: () => task.of(Message.fromData("foo")),
  });

  const getFoo = client.foo();
  expect(await getFoo()).toEqual("foo");
});

it("should be possible to cache responses via layer", async () => {
  type Api = {
    foo: () => task.Task<number>;
  };

  let count = 0;
  const client = create<Api>(
    {
      foo: () =>
        pipe(async () => {
          return Message.fromData(count++);
        }, task.map(Message.setMeta(() => ({ validThru: new Date(Date.now() + 3600) })))),
    },
    [Layer.timebasedCache]
  );

  const getFoo = client.foo();
  await getFoo();
  expect(await getFoo()).toEqual(0);
});

test("validation", async () => {
  type Api = {
    ping: (
      id: string
    ) => taskEither.TaskEither<"validation error", `pong: ${string}`>;
  };

  const client = create<Api>({
    ping: flow(
      Message.validate(z.string().min(5)),
      Message.setData(
        flow(
          either.map((data) => `pong: ${data}` as const),
          either.mapLeft(() => {
            return "validation error" as const;
          })
        )
      ),
      task.of
    ),
  });

  const getPing = client.ping("foo");

  expect(await getPing()).toEqual(either.left("validation error"));
});

test("authorization", async () => {
  type Api = {
    showSecret: () => taskEither.TaskEither<
      "unauthorized",
      `your secret is: ${string}`
    >;
  };

  const userRepository = {
    getUser: (userId: string): taskOption.TaskOption<{ secret: string }> =>
      pipe(
        userId === "existing user"
          ? option.some({ secret: "you like functional programming too much" })
          : option.none,
        task.of
      ),
  };

  const authorize = <Msg extends Message>(message: Msg) => {
    const getUser = match(message.meta)
      .with({ authorization: P.string }, ({ authorization }) =>
        pipe(
          userRepository.getUser(authorization),
          taskEither.fromTaskOption(() => "unauthorized" as const)
        )
      )
      .otherwise(() => taskEither.left("unauthorized" as const));

    return pipe(
      getUser,
      taskEither.map((user) =>
        pipe(
          message,
          Message.setMeta(() => ({ user }))
        )
      )
    );
  };

  let count = 0;

  const client = create<Api>(
    {
      showSecret: flow(
        task.of,
        task.chain(authorize),
        taskEither.map(({ meta }) => {
          return `your secret is: ${meta.user.secret}` as const;
        }),
        task.map(Message.fromData)
      ),
    },
    [
      Layer.mapRequest(
        Message.setMeta(() => ({
          authorization: count++ === 1 ? "existing user" : "non-existing user",
        }))
      ),
    ]
  );

  const showSecret = client.showSecret();

  expect(await showSecret()).toEqual(either.left("unauthorized"));
  expect(await showSecret()).toEqual(
    either.right("your secret is: you like functional programming too much")
  );
});
