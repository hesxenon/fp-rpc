import { array, reader, task } from "fp-ts";
import { pipe } from "fp-ts/lib/function";
import { ulid } from "ulid";
import {
  ApiDefinition,
  ApiFunction,
  Observable,
  Message,
  ignore,
  Layer,
  Handler,
} from "../Shared";

export namespace Client {
  export function create<Api extends ApiDefinition>({
    ws,
    layers = [],
  }: {
    ws: Pick<WebSocket, "onmessage" | "send" | "readyState" | "onopen">;
    /**
     * layers around each api function
     */
    layers?: Layer[];
  }): Api {
    const serialize = JSON.stringify;
    const parse = JSON.parse;

    const incoming = Observable.create<Message.Raw>((observer) => {
      ws.onmessage = (event) => pipe(event.data, parse, observer.next);
      return ignore;
    });

    const queue = [] as string[];

    ws.onopen = () => {
      let msg: string | undefined;
      while ((msg = queue.shift()) != null) {
        console.log("sending", msg)
        ws.send(msg);
      }
    };

    const plainHandler: Handler = reader.asks((topic) => {
      return task.chain(
        (message) => () =>
          new Promise((resolve) => {
            const messageId = ulid();

            const { unsubscribe } = incoming.subscribe(([id, _, message]) => {
              if (id !== messageId) {
                return;
              }
              resolve(message);
              unsubscribe();
            });

            const serialized = pipe([messageId, topic, message], serialize);

            if (ws.readyState === WebSocket.OPEN) {
              ws.send(serialized);
            } else {
              queue.push(serialized);
            }
          })
      );
    });

    const handler = pipe(
      layers,
      array.reduce(plainHandler, (current, middleware) => middleware(current))
    );

    const sendIt =
      (path: string[]) =>
      (data: any): task.Task<any> => {
        return pipe(
          handler(path.join("."))(task.of(Message.fromData(data))),
          task.map((msg) => msg.data)
        );
      };

    const proxify = (path: string[]) =>
      new Proxy(Object.assign(sendIt(path)), {
        get: (_, prop): ApiDefinition | ApiFunction => {
          return proxify([...path, prop as string]);
        },
      });

    return proxify([]);
  }
}
